import { combineReducers } from 'redux';

import taskFormReducer from './form/form.reducer';
import currentUser from './user/user.reducer';

const reducer = combineReducers({
  taskForm: taskFormReducer,
  currentUser: currentUser,
});

export default reducer;
