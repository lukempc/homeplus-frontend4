import _ from 'lodash';

const RatingCalculator = (reviews) => {
  const numOfReviews = reviews.length;
  const sumOfRating = reviews.reduce((total, { rating }) => (total = total + rating), 0) / numOfReviews;
  return { sumOfRating: _.round(sumOfRating, 1), numOfReviews: numOfReviews };
};

export default RatingCalculator;
