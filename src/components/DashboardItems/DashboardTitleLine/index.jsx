import React from 'react';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import Box from '@mui/material/Box';
import { ListItemText } from './DashboardTitleLine.style';
import CustomButton from '../../CustomButton';
import useUser from '../../../hooks/useUser';
import { setUser } from '../../../store/reducers/user/user.actions';

export default function BasicList({ title, edit, taskMode, disable }) {
  const { setTaskerMode } = useUser();
  const { user } = useSelector(state => state.currentUser);
  const dispatch = useDispatch();

  const handleTaskMode = () => {
    setTaskerMode(user.id);
    dispatch(setUser({...user, 'is_tasker': !user.is_tasker}));
  }
  return (
    <Box sx={{ width: '100%', bgcolor: 'background.paper' }}>
      <List sx={{ width: '95%', bgcolor: 'background.paper', marginLeft: '1rem' }}>
        <ListItem sx={{ height: '80px' }}>
          <ListItemText>
            <span>{title}</span>
            {edit ? (
              <CustomButton color="primary" variant="outlined" disabled={disable}>
                Edit
              </CustomButton>
            ) : null}
            {taskMode ? (
              <CustomButton color="primary" variant={user.is_tasker ? 'outlined' : 'contained'} onClick={handleTaskMode}>
                {`Turn ${user.is_tasker ? 'off' : 'on'} tasker mode`}
              </CustomButton>
            ) : null}
          </ListItemText>
        </ListItem>
        <Divider component="li" />
      </List>
    </Box>
  );
}

BasicList.propTypes = {
  title: PropTypes.string.isRequired,
  edit: PropTypes.bool,
  isTurn: PropTypes.bool,
  taskMode: PropTypes.bool,
  disable: PropTypes.bool,
};
