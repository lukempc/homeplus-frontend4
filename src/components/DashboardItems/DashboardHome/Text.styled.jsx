import styled from 'styled-components';

export const TextStyle = styled.div`
  display: flex;
  font-size: 18px;
  font-family: 'Sans-serif';
  text-indent: 50px;
  margin-top: 20px;
`;
