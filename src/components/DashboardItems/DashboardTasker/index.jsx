import React from 'react';
import PropTypes from 'prop-types';
import TaskCalendar from '../TaskCalendar';

const DashboardTasker = ({tasksOfUser, tasksOfTasker, isReloadData, setIsReloadData}) => (
  <>
    <TaskCalendar 
      tasksOfUser={tasksOfUser} 
      tasksOfTasker={tasksOfTasker}
      isReloadData={isReloadData} 
      setIsReloadData={setIsReloadData}  
    />
  </>
);

DashboardTasker.propTypes = {
  tasksOfUser: PropTypes.any,
  tasksOfTasker: PropTypes.any,
  isReloadData: PropTypes.bool,
  setIsReloadData: PropTypes.func,
};

export default DashboardTasker;
