import React, { useState } from 'react';
import PropTypes from 'prop-types';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';

const TaskerInput = ({ title, name, formData, setFormData }) => {

  const [isError, setError] = useState(false);

  const handleChange = (e) => {
    setError(false)
    e.preventDefault();
    setFormData({...formData, [e.target.name]: e.target.value});
    if (formData[name].length < 3) setError(true);
  }

  return (
    <Box sx={{ display: 'flex', flexDirection: 'row', margin: '15px' }}>
      <Typography sx={{ display: 'flex', marginRight: '20px', width: '150px' }}>{title}</Typography>
      <TextField 
        name={name} 
        onChange={handleChange}
        value={formData[name]}
        id="outlined-basic" 
        size="small" 
        multiline={name === 'title'? false : true}
        rows={4}
        sx={{ width: '450px' }}
        error={isError}
      />
    </Box>
  );
};

TaskerInput.propTypes = {
  title: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  formData: PropTypes.object.isRequired,
  setFormData: PropTypes.func.isRequired,
};

export default TaskerInput;
