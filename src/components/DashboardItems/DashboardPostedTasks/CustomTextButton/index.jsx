import React from 'react';
import PropTypes from 'prop-types';
import CustomButton from '../../../CustomButton';

const CustomTextButton = ({ children, ...props }) => (
  <CustomButton {...props} color="black" variant="text">
    {children}
  </CustomButton>
);

CustomTextButton.propTypes = {
  children: PropTypes.node.isRequired,
};

export default CustomTextButton;
