import React, {useCallback, useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
import { useSelector } from 'react-redux';
import Box from '@mui/material/Box';
import CustomTextButton from './CustomTextButton';
import TaskCard from '../../TaskCard';
import NoTask from '../DashboadDisableInfo/NoTask';

const DashboardPostedTasks = ({tasksOfUser, tasksOfTasker}) => {
    const currentUser = useSelector(state => state.currentUser);
    const [ displayTasks, setDisplayTasks ] = useState();
    const [ status, setStatus ] = useState(null);

    const handleStatus = (s) => {
      setStatus(s);
    }

    useEffect(() => {
      if(tasksOfUser) {
        if (displayTasks !== tasksOfUser) {
          setDisplayTasks(tasksOfUser);
        } 
      } else if(tasksOfTasker) {
        if (displayTasks !== tasksOfTasker) {
          setDisplayTasks(tasksOfTasker);
        }
      }
    }, [displayTasks, tasksOfUser, tasksOfTasker]);

  const tasksToDisplay = useCallback(
    (tasks) => {
      let filteredTasks = Object.values(tasks);
      
      const followingTasks = currentUser.followedTasks.map(task => task.id);
      filteredTasks = filteredTasks.map(task => {
        if (followingTasks.includes(task.id)) {
          return { ...task, 'following' : true }
        } else {
          return { ...task, 'following' : false }
        }
      });

      if (status) {
        return filteredTasks.filter((task) => task.task_status.includes(status));
      } 

      return filteredTasks
    },
    [status, currentUser]
  );

  return (
    <Box sx={{ display: 'flex', flexDirection: 'column' }}>
      { !_.isEmpty(displayTasks) ? <Box
        sx={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          width: '80%',
          alignItems: 'center',
          padding: '15px',
        }}
      >
        <CustomTextButton onClick={() => handleStatus('')}>All</CustomTextButton>
        <CustomTextButton onClick={() => handleStatus('open')}>Open</CustomTextButton>
        <CustomTextButton onClick={() => handleStatus('assigned')}>Assigned</CustomTextButton>
        <CustomTextButton onClick={() => handleStatus('completed')}>Completed</CustomTextButton>
      </Box>
      : null }
      <Box sx={{ marginTop: '30px', paddingLeft: '15px', display: 'inline-flex', flexWrap: 'wrap', gap: 3}}>
        { !_.isEmpty(displayTasks) ? tasksToDisplay(displayTasks)?.map(task => <TaskCard key={task.id} details={task} />) : <NoTask />}
      </Box>
    </Box>
  );
};

DashboardPostedTasks.propTypes = {
  tasksOfUser: PropTypes.any,
  tasksOfTasker: PropTypes.any,
};

export default DashboardPostedTasks;
