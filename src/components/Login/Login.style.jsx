import styled, { keyframes } from 'styled-components';
import Close from '@mui/icons-material/Close';

const jump = keyframes`
    from{
    transform: translateY(0)
    }
    to{
    transform: translateY(-3px)
    }
`;

export const Wrapper = styled.section`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  width: 100%;
  font-family: roboto, sans-serif;
`;

export const Form = styled.form`
  margin: 0 auto;
  width: 100%;
  max-width: 414px;
  padding: 1.5rem;
  display: flex;
  flex-direction: column;
  position: relative;
  button {
    position: relative;
    left: 50%;
    transform: translateX(-50%);
  }
`;

export const Input = styled.input`
  max-width: 100%;
  padding: 11px 13px;
  background: #f9f9fa;
  color: #444;
  margin-bottom: 0.9rem;
  border-radius: 4px;
  outline: 0;
  border: 1px solid rgba(245, 245, 245, 0.7);
  font-size: 14px;
  transition: all 0.3s ease-out;
  box-shadow: 0 0 3px rgba(0, 0, 0, 0.1), 0 1px 1px rgba(0, 0, 0, 0.1);
  :focus,
  :hover {
    box-shadow: 0 0 3px rgba(0, 0, 0, 0.15), 0 1px 5px rgba(0, 0, 0, 0.1);
  }
`;

export const Background = styled.div`
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.8);
  position: fixed;
  display: flex;
  justify-content: center;
  align-items: center;
`;

export const ModalWrapper = styled.div`
  width: 380px;
  height: 500px;
  box-shadow: 0 5px 16px rgba(0, 0, 0, 0.2);
  background: #fff;
  color: #000;
  display: grid;
  position: center;
  z-index: 10;
  border-radius: 10px;
`;

export const ModalContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  line-height: 1.8;
  color: #141414;
  p {
    margin-bottom: 1rem;
  }
`;
export const Button = styled.button`
  padding: 10px 24px;
  height: 35px;
  width: 80px;
  right: -230px;
  position: relative;
  background: #67bc98;
  color: #fff;
  text-align: center;
  border: none;
  background: #67bc98;
  border: none;
  border-radius: 3px;
  outline: 0;
  margin-bottom: 20px;
  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.1), 0 1px 2px rgba(0, 0, 0, 0.1);
  transition: all 0.3s ease-out;
  :hover {
    background: #7b7fda;
    animation: ${jump} 0.2s ease-out forwards;
  }
`;

export const CloseModalButton = styled(Close)`
  cursor: pointer;
  position: absolute;
  top: 20px;
  right: 20px;
  width: 32px;
  height: 32px;
  padding: 0;
  z-index: 10;
`;

export const Text = styled.div`
  color: #444;
  text-align: center;
  align-items: center;
  font-size: 13px;
  display: flex;
  button {
    left: 10%;
    top: -3px;
  }
`;
export const Link = styled.a`
  color: Black;
  text-align: center;
  font-size: 10px;
  display: inline-block;
`;
export const Container = styled.div`
  position: absolute;
  justify-content: center;
  align-items: center;
  align-above: 1px;
  z-index: 1000;
`;

export const ButtonSet = styled.div`
  justify-content: center;
  display: flex;
  flex-direction: row;
  button {
    position: relative;
    left: 0;
    transform: none;
  }
`;
