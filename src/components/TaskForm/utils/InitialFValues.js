import { postTaskFormModel } from './FormModel';

const {
  formFields: {
    category,
    title,
    date,
    certainTimeCheck,
    certainTime,
    budget,
    street,
    suburb,
    state,
    postcode,
    houseType,
    numOfRooms,
    numOfBath,
    levels,
    lift,
    inPerson,
    itemName,
    itemValue,
    description,
  },
} = postTaskFormModel;
const initialValues = {
  [category.name]: 'cleaning',
  [title.name]: '',
  [date.name]: new Date(),
  [certainTimeCheck.name]: false,
  [certainTime.name]: 0,
  [budget.name]: 0,
  [street.name]: '',
  [suburb.name]: '',
  [state.name]: 'NSW',
  [postcode.name]: '',
  [houseType.name]: '',
  [numOfRooms.name]: 0,
  [numOfBath.name]: 0,
  [levels.name]: 0,
  [lift.name]: false,
  [inPerson.name]: false,
  [itemName.name]: '',
  [itemValue.name]: 0,
  [description.name]: '',
};

export default initialValues;
