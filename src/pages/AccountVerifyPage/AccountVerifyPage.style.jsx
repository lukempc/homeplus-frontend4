import styled from 'styled-components';

export const Message = styled.div`
    max-width: 400px;
    position: relative;
    left: 50%; top:150px;
    font-size: 1.2rem;
    transform: translateX(-50%);
    > p{
        text-align: center;
    }
`;