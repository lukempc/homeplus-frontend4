import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import Dashboard from '../../components/Dashboard';
import useTasker from '../../hooks/useTasker';
import useTaskForm from '../../hooks/useTaskForm';
import { DashboardContainer } from './DashboardPage.style';

const DashboardPage = () => {
  const { user } = useSelector(state => state.currentUser);
  const { requestTasker, tasker } = useTasker();
  const { submitUserTaskSearch, submitTaskerTasksSearch, tasksByUser, tasksByTasker} = useTaskForm();
  const [ userInfo, setUserInfo ] = useState({});
  const [ taskerInfo, setTaskerInfo ] = useState(null);

  const [ tasksOfUser, setTasksOfUser ] = useState(null);
  const [ tasksOfTasker, setTasksOfTasker ] = useState(null);

  useEffect( () => {
    if(userInfo !== user) {
      setUserInfo(user);
      submitUserTaskSearch(user.id, '');
      if (user.is_tasker && user.is_tasker_data){
        requestTasker(user.id);
      }
    }
  }, [
    userInfo, 
    user, 
    requestTasker,
    submitUserTaskSearch
  ]);

  useEffect( () => {
    if (tasksByUser !== tasksOfUser){
      setTasksOfUser(tasksByUser);
    }
    if (tasksByTasker !== tasksOfTasker) {
      setTasksOfTasker(tasksByTasker);
    }
  }, [tasksByUser, tasksOfUser, tasksOfTasker, tasksByTasker]);

  useEffect(() => {
    if(taskerInfo !== tasker) {
      setTaskerInfo(tasker);
      submitTaskerTasksSearch(tasker.id, '');
    }
  }, [tasker, taskerInfo, submitTaskerTasksSearch]);

  
  return (
    <DashboardContainer>
      <Dashboard 
        tasksOfUser={tasksOfUser} 
        tasksOfTasker={tasksOfTasker}
      />
    </DashboardContainer>
  );
};

export default DashboardPage;
