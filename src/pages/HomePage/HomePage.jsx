import React from 'react';
import HomePageInfo from './HomePageInfo';
import HomePageInstruction from './HomePageInstruction';
import Footer from '../../components/Footer';

const HomePage = () => (
  <div>
    <HomePageInfo />
    <HomePageInstruction />
    <Footer />
  </div>
);

export default HomePage;
