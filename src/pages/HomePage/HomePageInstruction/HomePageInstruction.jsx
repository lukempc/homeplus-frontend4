import React from 'react';
import HomePageCard from '../../../components/HomepageCard';
import { Box, Container } from './HomePageInstruction.style';
import HomeSteps from '../HomePageTaskStep';

const HomePageTaskType = () => (
  <Box>
    <h3>Post your first task in seconds</h3>
    <Container>
      <HomePageCard type="Cleaning" />
      <HomePageCard type="Removal" />
      <HomePageCard type="Handyperson" />
    </Container>
    <h3>Follow steps to get your tasks in done</h3>
    <HomeSteps />
  </Box>
);

export default HomePageTaskType;
