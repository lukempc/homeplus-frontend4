import React, { useEffect, useState } from 'react';
import { useNavigate } from 'react-router-dom';
import { useParams } from 'react-router-dom';
import TextField from '@mui/material/TextField';
import CustomButton from '../../components/CustomButton';
import { ResetBox } from './ResetPasswordPage.style';
import useUser from '../../hooks/useUser';

const ResetPasswordPage = () => {
  const { email } = useParams();
  const { resetPassword } = useUser();
  const navigate = useNavigate();
  const [passwordError, setPasswordError] = useState(false);
  const [passwordFormatError, setPasswordFormatError] = useState(false);
  const [passwardCheck, setPasswordCheck] = useState('');
  const passwordRegex = /^(?=.*\d)(?=.*[a-zA-Z]).{8,}$/;
  const initData = {
    email: '',
    password: '',
    repassword: '',
  };
  const [userInfo, setUserInfo] = useState(initData);

  useEffect(() => {
    if (email && userInfo.email !== email) {
      setUserInfo({ ...userInfo, email: email });
    }
  }, [email, userInfo]);

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (passwordError || passwordFormatError || Object.values(userInfo).includes('')) return;
    const res = await resetPassword(userInfo);
    if (res === 'success') {
      alert('Reset success');
      navigate('/', { replace: true });
    }
  };

  const handleRepasswordChange = (e) => {
    e.preventDefault();
    setUserInfo({ ...userInfo, [e.target.name]: e.target.value });
    if (userInfo.password && userInfo.password === e.target.value) {
      setPasswordError(false);
      setPasswordCheck('');
    } else {
      setPasswordError(true);
      setPasswordCheck("password doesn't match!");
    }
  };

  const handlePasswordChange = (e) => {
    e.preventDefault();
    setUserInfo({ ...userInfo, [e.target.name]: e.target.value });
    if (e.target.value.match(passwordRegex)) {
      setPasswordFormatError(false);
    } else {
      setPasswordFormatError(true);
    }
  };

  return (
    <ResetBox>
      <h3>Reset your passeord</h3>
      <TextField
        type="password"
        name="password"
        placeholder="Enter your password..."
        value={userInfo.password}
        onChange={handlePasswordChange}
        error={passwordFormatError}
        helperText={passwordFormatError ? 'minimum 8 characters long & 1 non-letter character' : null}
      />
      <br />
      <TextField
        type="password"
        name="repassword"
        placeholder="Re-enter your password..."
        value={userInfo.repassword}
        onChange={handleRepasswordChange}
        error={passwordError}
        helperText={passwordError && passwardCheck}
      />
      <CustomButton variant="contained" size="large" onClick={handleSubmit}>
        submit
      </CustomButton>
    </ResetBox>
  );
};

export default ResetPasswordPage;
